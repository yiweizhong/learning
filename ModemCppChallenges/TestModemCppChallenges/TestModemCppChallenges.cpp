#define BOOST_TEST_MODULE TestModemCppChallenges
#include <boost/test/included/unit_test.hpp>


//https://stackoverflow.com/questions/59008647/unresolved-external-symbol-x-using-boost-tests-from-one-visual-studio-project
#include "../ModemCppChallenges/math1.h"
#include "../ModemCppChallenges/math1.cpp"

#include "../ModemCppChallenges/lf.h"
#include "../ModemCppChallenges/lf.cpp"

#include "../ModemCppChallenges/matrix.h"
#include "../ModemCppChallenges/matrix.cpp"

#include "../ModemCppChallenges/resource.h"
#include "../ModemCppChallenges/resource.cpp"

namespace tt = boost::test_tools;


BOOST_AUTO_TEST_SUITE(TestProjectSetups)

BOOST_AUTO_TEST_CASE(Test_test_project_setup)
{
	int i = 1;
	BOOST_TEST(i);
	BOOST_TEST(i == 1);
}

BOOST_AUTO_TEST_CASE(Test_test_project_plumbing)
{
	auto i = add(2, 3);
	BOOST_TEST(i == 5);
}

BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(MathProblems)

BOOST_AUTO_TEST_CASE(Test_sumAllNumsDivisableBy3Or5)
{
	auto result = sumAllNumsDivisableBy3Or5(10);
	int expected = 33;
	BOOST_TEST(result == expected);
}

BOOST_AUTO_TEST_CASE(Test_findMaxCommonDivisor)
{
	//BOOST_TEST(findMaxCommonDivisor(15 * 11, 66) == 33);
	BOOST_TEST(findMaxCommonDivisor(6, 8) == 2);

}


BOOST_AUTO_TEST_CASE(Test_findSmallestCommonMultiplier)
{
	BOOST_TEST(findSmallestCommonMultiplier(vector<int>{1,2,3}) == 6);
	BOOST_TEST(findSmallestCommonMultiplier(vector<int>{8,6,6}) == 24);
	BOOST_TEST(findSmallestCommonMultiplier(vector<int>{8,6,9}) == 72);

}


BOOST_AUTO_TEST_CASE(Test_findLargestPrimeLessThanGivenNumber)
{
	BOOST_TEST(findLargestPrimeLessThanGivenNumber(6) == 5);
	BOOST_TEST(findLargestPrimeLessThanGivenNumber(4) == 3);
	BOOST_TEST(findLargestPrimeLessThanGivenNumber(12) == 11);

}

BOOST_AUTO_TEST_CASE(Test_findValueOfPie)
{
	BOOST_TEST(findValueOfPie(100000) == 3.141, tt::tolerance(0.001));
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(LanguageFeature)

BOOST_AUTO_TEST_CASE(Test_ipv4_class)
{
	ipv4 ip(1,2,3,4);

	std::cout << "ip: " << ip << std::endl;

	auto ip2 = ip;

	ip2.update_section(2, 99);

	std::cout << "ip: " << ip << std::endl;

	std::cout << "ip2: " << ip2 << std::endl;

	auto ip3(ip);

	std::cout << "ip3 after copying: " << ip3 << std::endl;

	ip3.update_section(2, 11);

	std::cout << "ip: " << ip << std::endl;

	std::cout << "ip3 after value changes: " << ip3 << std::endl;

	ipv4 ip5;

	std::cout << "ip5 before user inputs: " << ip5 << std::endl;

	ip5.update_section("10.20.30.40");

	std::cout << "ip5 after user inputs: " << ip5 << std::endl;

}

BOOST_AUTO_TEST_CASE(Test_list_ipv4_between)
{
	//std::cout << "from ip (decimal): " << from_ip.to_base_10() << std::endl;
	//std::cout << "5 % 7: " << 5 / 7 << std::endl;
	//std::cout << "7 % 7: " << 7 / 7 << std::endl;
	//std::cout << "7 % 5: " << 7 / 5 << std::endl;
	//std::cout << "7 % -7: " << 7 / -7 << std::endl;

	
	BOOST_TEST(ipv4(33489919).to_base_10() == 33489919);
	std::cout << "ip from decimal 33489919: " << ipv4(33489919) << std::endl;
	
	BOOST_TEST(ipv4(16777983).to_base_10() == 16777983);
	std::cout << "ip from decimal 16777983: " << ipv4(16777983) << std::endl;


	ipv4 from_ip(1, 255, 254, 250);
	ipv4 to_ip(1, 255, 255, 3);

	std::cout << "from ip: " << from_ip << std::endl;
	std::cout << "to ip: " << to_ip << std::endl;
	list_ips(from_ip, to_ip);

}

BOOST_AUTO_TEST_CASE(Test_matrix)
{
	{
		matrix<double> a(2, 3, std::vector<double>{1, 2, 3, 4, 5, 6});
		BOOST_TEST(a.size(1) == 2);
		BOOST_TEST(a.size(2) == 3);
		BOOST_TEST(a(0, 0) == 1);
		BOOST_TEST(a(0, 1) == 2);
		BOOST_TEST(a(0, 2) == 3);
		BOOST_TEST(a(1, 0) == 4);
		BOOST_TEST(a(1, 1) == 5);
		BOOST_TEST(a(1, 2) == 6);
	}
	{
		matrix<double> a(6, 1, std::vector<double>{1, 2, 3, 4, 5, 6});
		BOOST_TEST(a.size(1) == 6);
		BOOST_TEST(a.size(2) == 1);
		BOOST_TEST(a(0, 0) == 1);
		BOOST_TEST(a(1, 0) == 2);
		BOOST_TEST(a(2, 0) == 3);
		BOOST_TEST(a(3, 0) == 4);
		BOOST_TEST(a(4, 0) == 5);
		BOOST_TEST(a(5, 0) == 6);
	}

	{
		matrix<double> a(1, 6, std::vector<double>{1, 2, 3, 4, 5, 6});
		BOOST_TEST(a.size(1) == 1);
		BOOST_TEST(a.size(2) == 6);
		BOOST_TEST(a(0, 0) == 1);
		BOOST_TEST(a(0, 1) == 2);
		BOOST_TEST(a(0, 2) == 3);
		BOOST_TEST(a(0, 3) == 4);
		BOOST_TEST(a(0, 4) == 5);
		BOOST_TEST(a(0, 5) == 6);
	}

	{
		matrix<double> a(1, 2, std::vector<double>{1, 2});
		std::cout << a;
		BOOST_TEST(a.is_empty() == false);

		std::cout << "before b stealing a: " << std::endl;
		//matrix<double> b(std::move(a));
		matrix<double> b(1, 1, std::vector<double>{1});

		b = std::move(a);
		std::cout << b;
		
		std::cout << "after b stealing a: " << std::endl;
		
		
		b.fill(100);
		std::cout << "b after fill with 100: "<< std::endl << b;

		matrix<double> c(1, 2, std::vector<double>{1, 2});
		std::cout << "c: " << std::endl << b;

		b.swap(c);
		std::cout << "b after swapping with c: " << std::endl << b;
		std::cout << "c after swapping with b: " << std::endl << c;

	}


}

BOOST_AUTO_TEST_CASE(Test_mimimum)
{
	
	BOOST_TEST(minimum(1, 2, 3, 4) == 1);
	BOOST_TEST(minimum(10, 20) == 10);


	BOOST_TEST(minimum_c(std::less<>(), 1, 2, 3, 4) == 1);
	BOOST_TEST(minimum_c(std::less<>(), 10, 20) == 10);
}


BOOST_AUTO_TEST_CASE(Test_populate_varadiac_parms_to_container)
{

	std::vector<int> container;
	BOOST_TEST(container.size() == 0);
	populate(container, 1, 2, 3, 4);
	BOOST_TEST(container.size() == 4);

	auto print = [](const int& n) { std::cout << " " << n; };
	std::for_each(container.begin(), container.end(), print);
	
}


BOOST_AUTO_TEST_CASE(Test_check_container_with_varadiac_parms)
{

	std::vector<int> v{ 1, 2, 3, 4, 5, 6 };
	
	BOOST_TEST(contains_all(v, 1, 3) == true);
	BOOST_TEST(contains_all(v, 0, 3, 30) == false);


	BOOST_TEST(contains_any(v, 1, 3) == true);
	BOOST_TEST(contains_any(v, 0, 3, 30) == true);

	BOOST_TEST(contains_none(v, 1, 3) == false);
	BOOST_TEST(contains_none(v, 0, 3, 30) == false);
	BOOST_TEST(contains_none(v, 99, 13, 31) == true);

}

BOOST_AUTO_TEST_CASE(Test_resource_wrapper)
{

	//resource_wrapper a = resource_wrapper{ "resource 1" };
	

	resource_wrapper b = resource_wrapper(resource("resource 2"));

	std::cout << "before end" << std::endl;

}



BOOST_AUTO_TEST_SUITE_END()
