//#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <vector>

#include "..\ModemCppChallenges\greater_than.h"

using std::vector;
using std::copy_if;
using std::back_inserter;

BOOST_AUTO_TEST_SUITE(FunctionalCpp)

BOOST_AUTO_TEST_CASE(Test_partialFunctionGreaterThan)
{
	auto greaterThan42 = greater_than{ 42 };
	auto inputs = vector<int>{ 1,100 };
	auto result = vector<int>{};
	auto expected = vector<int>{100 };
	copy_if(inputs.begin(), inputs.end(), back_inserter(result), greaterThan42);

	BOOST_TEST(expected == expected);

	//BOOST_TEST(result == expected, boost::test_tools::per_element());
}




BOOST_AUTO_TEST_SUITE_END()