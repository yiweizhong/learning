#include <boost/test/unit_test.hpp>

#include "..\ModemCppChallenges\ModernComputationalFinance\Matrix1.h"
#include "..\ModemCppChallenges\ModernComputationalFinance\Vector1.h"



BOOST_AUTO_TEST_SUITE(ModernComputationalFinance_CacheMemoryOptimisation)

BOOST_AUTO_TEST_CASE(Test_Matrix1Creation)
{
	auto matrix1Obj = Matrix1<double>(2, 3);
	matrix1Obj[1][1] = 1;

	std::cout << "data: " << std::endl << matrix1Obj;

	BOOST_TEST(matrix1Obj.rows() == 2);
	BOOST_TEST(matrix1Obj.cols() == 3);
}


BOOST_AUTO_TEST_CASE(Test_Matrix1ProductNaive)
{
	auto left = Matrix1<double>(1000, 1000, 1);
	auto right = Matrix1<double>( 1000, 1000, 1);
	auto expected = Matrix1<double>(1000, 1000, 1000);
	auto result = matrix1ProductNaive(left, right);

	//std::cout << "data: " << std::endl << result;
	
	BOOST_TEST(expected == result);

	//5.4 sec
}

BOOST_AUTO_TEST_CASE(Test_Matrix1ProductOptimised)
{
	auto left = Matrix1<double>(1000, 1000, 1);
	auto right = Matrix1<double>(1000, 1000, 1);
	auto expected = Matrix1<double>(1000, 1000, 1000);
	auto result = matrix1ProductOptimised(left, right);

	//std::cout << "data: " << std::endl << result;

	BOOST_TEST(expected == result);

	//2.2 sec!!! The performance improvement comparing the naive implementation
	//comes from taking advanrage of cache memeory preload and SIMD
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(ModernComputationalFinance_MoveSemantics)

BOOST_AUTO_TEST_CASE(Test_Vector1Creation)
{
	auto vec1 = Vector1<double>(); //calling constructor with default parameter
	//https://en.cppreference.com/w/cpp/language/default_arguments

	BOOST_TEST(vec1.Size() == 0);
	BOOST_TEST(vec1.Ptr() == nullptr);

	auto vec2 = Vector1<double>(2); //calling constructor with default parameter
	//https://en.cppreference.com/w/cpp/language/default_arguments

	BOOST_TEST(vec2.Size() == 2);
	BOOST_TEST(vec2.Ptr() != nullptr);

}


BOOST_AUTO_TEST_CASE(Test_Vector1CopyConstructor)
{
	auto vec1 = Vector1<double>(2); 
	vec1.Ptr()[0] = 1.;
	vec1.Ptr()[1] = 10.;
	
	auto vec2 = Vector1(vec1); //calling copy constructor
	
	BOOST_TEST(vec2.Size() == 2);
	BOOST_TEST(vec2.Ptr()[0] == 1.);
	BOOST_TEST(vec2.Ptr()[1] == 10.);


}


BOOST_AUTO_TEST_CASE(Test_Vector1CopyAssignment)
{
	auto vec1 = Vector1<double>(2);
	vec1.Ptr()[0] = 1.;
	vec1.Ptr()[1] = 10.;

	auto vec2 = Vector1<double>(); 

	vec2 = vec1;

	BOOST_TEST(vec2.Size() == 2);
	BOOST_TEST(vec2.Ptr()[0] == 1.);
	BOOST_TEST(vec2.Ptr()[1] == 10.);

}

BOOST_AUTO_TEST_CASE(Test_Vector1Resize)
{
	auto vec1 = Vector1<double>(2);
	vec1.Ptr()[0] = 1.;
	vec1.Ptr()[1] = 10.;

	vec1.resize(3);

	BOOST_TEST(vec1.Size() == 3);
	BOOST_TEST(vec1.Ptr()[0] == 1.);
	BOOST_TEST(vec1.Ptr()[1] == 10.);

}


BOOST_AUTO_TEST_CASE(Test_Vector1MoveConstructor)
{
	auto vec1 = Vector1<double>(2, "vec1");
	vec1.Ptr()[0] = 1.;
	vec1.Ptr()[1] = 10.;

	auto vec2 = Vector1(std::move(vec1));

	BOOST_TEST(vec2.Size() == 2);
	BOOST_TEST(vec2.Ptr()[0] == 1.);
	BOOST_TEST(vec2.Ptr()[1] == 10.);

	BOOST_TEST(vec1.Size() == 0);
	BOOST_TEST(vec1.Ptr() == nullptr);

}

BOOST_AUTO_TEST_CASE(Test_Vector1MoveAssignment)
{
	auto vec1 = Vector1<double>(2, "vec1");
	vec1.Ptr()[0] = 1.;
	vec1.Ptr()[1] = 10.;

	auto vec2 = Vector1<double>();

	vec2 = std::move(vec1);

	BOOST_TEST(vec2.Size() == 2);
	BOOST_TEST(vec2.Ptr()[0] == 1.);
	BOOST_TEST(vec2.Ptr()[1] == 10.);


}


BOOST_AUTO_TEST_SUITE_END()
