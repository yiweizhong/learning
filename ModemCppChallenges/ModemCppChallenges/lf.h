#pragma once

#include <vector>
#include <stdlib.h>
#include <array>
#include <iostream>
#include <sstream>


class ipv4 {
	std::array<unsigned char, 4> sections;
public:
	ipv4();
	ipv4(unsigned char a, unsigned char b, unsigned char c, unsigned char d);
	ipv4(ipv4 const& other);
	ipv4(int decimal_val);
	ipv4& operator=(ipv4 const& other);
	void update_section(int idx, char val);
	void update_section(std::string str_ip);
	int section(int idx);
	int to_base_10();


	//not a class member
	friend std::ostream& operator<<(std::ostream& os, const ipv4& a);
	//not a class member
	friend std::istream& operator>>(std::istream& is, ipv4& a);

};

void list_ips(ipv4 from_ip, ipv4 to_ip);