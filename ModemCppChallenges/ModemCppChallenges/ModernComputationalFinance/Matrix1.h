#pragma once
#include <vector>
#include <iostream>

template <class T>
class Matrix1 {
private:
	
	//dimensions
	size_t myRows;
	size_t myCols;

	//data storage
	std::vector<T> myVector;

public:
	
	using value_type = T;

	//constructor
	Matrix1() :myRows(0), myCols(0) {}
	Matrix1(const size_t rows, const size_t cols) 
		:myRows(rows), myCols(cols), myVector(rows* cols) {}
	Matrix1(const size_t rows, const size_t cols, const T val)
		:myRows(rows), myCols(cols), myVector(rows* cols, val) {}

	//access
	size_t rows() const { return myRows; }
	size_t cols() const { return myCols; }

	//access element via matrix[i][j]
	T* operator[] (const size_t row) {
		return &myVector[myCols * row];
	}

	const T* operator[] (const size_t row) const {
		return &myVector[myCols * row];
	}

	//with friend the method can access the private field of the object
	//https://stackoverflow.com/questions/4660123/overloading-friend-operator-for-template-class
	template <typename U>
	friend std::ostream& operator<<(std::ostream& os, const Matrix1<U>& a)
	{
		for (size_t i = 0; i < a.rows(); i++) {
			for (size_t j = 0; j < a.cols(); j++) {
				if (j > 0) {
					os << ",";
				}
				os << a[i][j];
			}
			os << ";" << std::endl;
		}
		return os;
	}

	template <typename U>
	friend bool operator==(const Matrix1<U>& a, const Matrix1<U>& b)
	{
		auto result = false;
		if (a.rows() == b.rows() && a.cols() == b.cols())
		{
			for (int i = 0; i < a.rows(); i++) {
				for (int j = 0; j < a.cols(); j++) {
					if (a[i][j] != b[i][j])
						break;
				}
			}
			result = true;
		}
		return result;
	}


	

};

inline Matrix1<double> matrix1ProductNaive(
	const Matrix1<double>& left, 
	const Matrix1<double>& right) {

	//empty output object
	auto rows = left.rows();
	auto cols = right.cols();
	auto rc = left.cols(); //or right.rows() they have to match to do product
	auto result = Matrix1<double>(rows, cols);
	
	for (size_t i = 0; i < rows; i++) {
		for (size_t j = 0; j < cols; j++) {
			for (size_t k = 0; k < rc; k++) {
				//the right[k][j] is the performance bottleneck as it will jump from 
				//row to row. If the row is long then the cache memory is wasted
				result[i][j] += left[i][k] * right[k][j];
			}
		}
	}
	return result;
}


inline Matrix1<double> matrix1ProductOptimised(
	const Matrix1<double>& left,
	const Matrix1<double>& right) {

	//empty output object
	auto rows = left.rows();
	auto cols = right.cols();
	auto rc = left.cols(); //or right.rows() they have to match to do product
	auto result = Matrix1<double>(rows, cols);

	for (size_t i = 0; i < rows; i++) {
		for (size_t k = 0; k < rc; k++) {
			for (size_t j = 0; j < cols; j++)
			{
				result[i][j] += left[i][k] * right[k][j];
			}
		}	
	}
	return result;
}