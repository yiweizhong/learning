#pragma once
#include <iostream>
#include <string>

//a dummy homemade vector class with copy and move 
template<class T>
class Vector1 {

private:
	std::string myName;
	T* myPtr;
	size_t mySize;

public:
	Vector1(size_t size = 0, std::string name = "unknown") :
		mySize(size), 
		myName(name),
		myPtr(size>0 ? new T[size] : nullptr) {

	}

	//copy constructor
	Vector1(const Vector1& rhs):
		mySize(rhs.mySize),
		myName(rhs.myName),
		myPtr(rhs.mySize > 0 ? new T[rhs.mySize] : nullptr) //need to use the rhs size because the parameter init sequence isnt guaranteeed
	{
		
		std::copy(rhs.myPtr, rhs.myPtr + rhs.mySize, myPtr);
		std::cout << myName << " copy constructor is called" << std::endl;
	}

	//copy assignment
	Vector1& operator=(const Vector1& rhs) {
		if (this != &rhs) {
			auto temp = Vector1(rhs);
			swap(temp);
			std::cout << "copy assignment is called with temp swap idiom to copy from " << myName << std::endl;
		}
		else {
			std::cout << "copy assignment is skipped from assigning to itself" << std::endl;
		}
		return *this; //this is a pointer. it needs to be dereferenced and reference is automatically taken. 
	}

	//move constructor
	Vector1(Vector1&& rhs) noexcept
	{
		swap(rhs);
		rhs.myPtr = nullptr;
		std::cout << "move constructor is called to move " << myName << std::endl;
	}

	//move assignment
	Vector1& operator=(Vector1&& rhs) noexcept {
		if (this != &rhs) {
			auto temp = Vector1(std::move(rhs));
			swap(temp); //the existing local resources wont be released if swap with rhs directly. 
			            //rhs might be lvalue will continue to exist
			std::cout << "move assignment is called to swap to " << myName << std::endl;
		}
		else {
			std::cout << "move assignment is skipped from assigning to itself" << std::endl;
		}
		return *this; //this is a pointer. it needs to be dereferenced and reference is automatically taken. 
	}


	~Vector1() {
		delete[] myPtr;
		std::cout << myName << " destructor is called" << std::endl;
	}

	void swap(Vector1& rhs) {
		std::swap(myPtr, rhs.myPtr);
		std::swap(mySize, rhs.mySize);
		std::swap(myName, rhs.myName);
	}



	void resize(size_t size) {
		if (mySize != size) {
			auto temp = Vector1(size);
			std::copy(myPtr, myPtr + mySize, temp.myPtr);
			swap(temp);
		}
	}

	T& operator[](size_t i) { return myPtr[i]; } //no size check
	const T& operator[](size_t i) const { return myPtr[i]; } //no size check

	T* begin() { return myPtr; }
	const T* begin() const { return myPtr; }

	T* end() { return myPtr + mySize; }
	const T* end() const { return myPtr + mySize; }

	const size_t Size() const { return mySize; }
	const std::string Name() const { return myName; }
	T* Ptr() { return myPtr; }


};


