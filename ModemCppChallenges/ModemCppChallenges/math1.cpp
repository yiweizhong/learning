#include "math1.h"

#include <random>
#include <stdlib.h>
#include <numeric>
#include <set>


using std::set;
using std::max;
using std::min;
using std::accumulate;





bool is_prime(int const num)
{
	if (num <= 3) { return num > 1; }
	else if (num % 2 == 0 || num % 3 == 0)
	{
		return false;
	}
	else
	{
		for (int i = 5; i * i <= num; i += 6)
		{
			if (num % i == 0 || num % (i + 2) == 0)
			{
				return false;
			}
		}
		return true;
	}
}




int sumAllNumsDivisableBy3Or5(int limit)
{
	//use the smaller of the 2 dividers to figure out the rough number of loop
	auto result = div(limit, 3);
	auto numberSet = set<int>{};

	for (int i = 1; i <= result.quot; i++) {
		auto a = 3 * i;
		auto b = 5 * i;
		
		if (a <= limit) {
			numberSet.insert(a);
		}
		if (b <= limit) {
			numberSet.insert(b);
		}
	}

	return accumulate(numberSet.begin(), numberSet.end(), 0);

}

int findMaxCommonDivisor(int a, int b)
{
	/*
	if (a == b && a !=0) {
		return a;
	}else if (a == 0) {
		return b;
	}
	else if (b == 0) {
		return a;
	}
	else {
		auto reduction = div(max(a,b), min(a, b));
		return findMaxCommonDivisor(min(a, b), reduction.rem);
	}
	*/
	

	//The following is more efficient as 6 % 8 = 6 so essentially 
	//the order of a and b does not matter. the function will turn 
	//the 2 numbers around before mod can happen

	return b == 0 ? a : findMaxCommonDivisor(b, a % b);
}

int findSmallestCommonMultiplier(vector<int> nums)
{
	auto l = nums.size();
	if (l == 1)
	{
		return nums[0];
	}
	else
	{
		int result = nums[0] * nums[1] / findMaxCommonDivisor(nums[0], nums[1]);
		if (l == 2)
		{
			return result;
		}else{
			
			for (size_t i = 2; i < l; i++) {
				result = result * nums[i] / findMaxCommonDivisor(result, nums[i]);
			}
			return result;
		}
	}
}

int findLargestPrimeLessThanGivenNumber(int limit)
{
	for (int i = limit; i > 1; i--)
	{
		if (is_prime(i))
		{
			return i;
		}
	}

	return 0;
}

double findValueOfPie(int nSamples)
{
	std::random_device rd; //a random device will be used to generate a seed
	std::mt19937 gen(rd()); //standard merseene_twister engine with seed from rd
	std::uniform_real_distribution<> uniform_dist(-1, 1);
	int sample_inside_circle = 0;
	const double r = 1; 

	for (int i = 0; i < nSamples; i++) {
		//draw x and y
		double x = uniform_dist(gen);
		double y = uniform_dist(gen);
		double l = sqrt(x * x + y * y);
		if (l <= r) {
			sample_inside_circle ++;
		}
	}
	double square_area = pow(r * 2, 2);
	double circle_pct_square = (double)sample_inside_circle / (double)nSamples;
	double circle_area = square_area * circle_pct_square;
	double pie = circle_area / (r * r);

	return pie;
}


