#include "resource.h"


resource& resource::operator=(resource&& other) noexcept
{
	// TODO: insert return statement here
	if (this == &other) {
		std::cout << "can't move [" << name << "] to itself" << std::endl;
	}

	//std::cout << "[" << other.name << "] is being moved" << std::endl;
	std::cout << "[" << other.name << "] is being moved" << std::endl;

	name = other.name;
	other.name = "";

	return *this;
	
}

resource::resource(const std::string& name):name(name){
	std::cout << "[" << name << "] is constructed" << std::endl; 
}
resource::~resource() { 
	std::cout << "[" << name << "] is destroyed" << std::endl;
}

resource_wrapper::resource_wrapper(const std::string& name){
	rsc = new resource(name);
}

resource_wrapper::resource_wrapper(resource&& other)
{
	*rsc = std::move(other);
	std::cout << "after resource_wrapper constructor(resource&&)" << std::endl;
}

resource_wrapper::~resource_wrapper(){
	std::cout << "from resource_wrapper destructor" << std::endl;
	delete rsc;
}
