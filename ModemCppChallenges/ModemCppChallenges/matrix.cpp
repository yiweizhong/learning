#include "matrix.h"
#include <functional>

//private
template <class T>
int matrix<T>::offset(int x, int y) const
{
	if (x < 0 || x >= dim_x)
		throw std::overflow_error("x overflow");
	if (y < 0 || y >= dim_y)
		throw std::overflow_error("y overflow");

	return dim_y * x + y;
}

//public
template <class T>
matrix<T>::matrix(size_t dim_x, size_t dim_y, std::vector<T> vals):
	dim_x(dim_x), dim_y(dim_y)
{
	if (vals.size() != (dim_x * dim_y))
		throw std::overflow_error("dimension and init values incompatible");
	data = vals;
}

template <class T>
matrix<T>::matrix(matrix&& other) noexcept
{
	data = std::move(other.data);
	dim_x = other.dim_x;
	dim_y = other.dim_y;

	std::cout << "move constructor called for matrix" << std::endl;

}

template <class T>
matrix<T>& matrix<T>::operator=(matrix<T>&& other) noexcept
{
	// TODO: insert return statement here
	data = std::move(other.data);
	dim_x = other.dim_x;
	dim_y = other.dim_y;

	std::cout << "move assignment called for matrix" << std::endl;

	return *this;
}


template <class T>
int matrix<T>::size(int rank) const
{
	if (rank == 1)
		return dim_x;
	else if (rank == 2)
		return dim_y;
	else
		throw std::out_of_range("rank is out of range");
}

template <class T>
T matrix<T>::operator()(size_t x, size_t y) const
{
	return data[offset(x,y)];
}

template<class T>
bool matrix<T>::is_empty() const
{
	return (dim_x == 0 && dim_y == 0);
}

template<class T>
void matrix<T>::fill(T filler)
{
	//good
	std::fill(begin(), end(), filler);
}

template<class T>
void matrix<T>::swap(matrix& other)
{
	data.swap(other.data);
}



//https://stackoverflow.com/questions/4660123/overloading-friend-operator-for-template-class
template <typename T>
std::ostream& operator<<(std::ostream& os, const matrix<T>& a)
{
	// TODO: insert return statement here
	for (int i = 0; i < a.dim_x; i++) {
		for (int j = 0; j < a.dim_y; j++) {
			if(j>0)
				os << ",";
			os << a(i, j);
		}
		os << ";" << std::endl;
	}

	return os;
}


//
template<typename T>
T minimum(T const a, T const b) { return a < b ? a : b; };

template<typename T1, typename ...T>
T1 minimum(T1 a, T ...args) {
	//Parameter pack expansion: expands to comma-separated 
	//list of zero or more patterns. Pattern must include at least one parameter pack.
	return minimum(a, minimum(args...));
}

//
template<class Compare, typename T>
T minimum_c(Compare comp, T const a, T const b) {return comp(a, b) ? a : b;}

template<class Compare, typename T1, typename... T>
T1 minimum_c(Compare comp, T1 a, T... args) {
	return minimum_c(comp, a, minimum_c(comp, args...));
}


//adding a range of values to a container
template<typename T, typename... T1>
void populate(std::vector<T>& container, T1&& ...args) {
	//(container.push_back(std::forward<T1>(args)), ...);
	(container.push_back(std::forward<T1>(args)), ...);
}

//https://stackoverflow.com/questions/49859514/c-unary-right-fold-vs-unary-left-fold-with-comma-operator

template<typename T, typename... Args>
bool contains_all(std::vector<T>& container, Args&& ...args) {
	return ((std::find(container.begin(), container.end(), args) != container.end()) && ...);
}

template<typename T, typename... Args>
bool contains_any(std::vector<T>& container, Args&& ...args) {
	return ((std::find(container.begin(), container.end(), args) != container.end()) || ...);
}

template<typename T, typename... Args>
bool contains_none(std::vector<T>& container, Args&& ...args) {
	return ! ((std::find(container.begin(), container.end(), args) != container.end()) || ...);
}
