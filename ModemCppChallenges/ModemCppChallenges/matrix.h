#pragma once

#include <stdexcept>
#include <vector>
#include <iostream>


template <class T>
class matrix {
private:
	typedef T value_type;
	typedef value_type* iterator;
	
	size_t dim_x;
	size_t dim_y;
	std::vector<T> data;
	int offset(int x, int y) const;
public:
	matrix() = delete;
	matrix(size_t dim_x, size_t dim_y, std::vector<T> vals);
	matrix(matrix&& other) noexcept;
	matrix& operator=(matrix&& other) noexcept;
	int size(int rank) const;
	T operator()(size_t x, size_t y) const;
	bool is_empty() const;
	void fill(T filler);
	iterator begin() { return data.data(); }
	iterator end() { return data.data() + data.size();}
	void swap(matrix& other);


	//https://stackoverflow.com/questions/4660123/overloading-friend-operator-for-template-class
	template <typename U>
	friend std::ostream& operator<<(std::ostream& os, const matrix<U>& a);
	
};

