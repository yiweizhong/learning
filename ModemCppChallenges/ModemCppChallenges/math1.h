#pragma once

#include <vector>

using std::vector;

inline int add(int a, int b) {
	return a + b;
}

int sumAllNumsDivisableBy3Or5(int limit);
int findMaxCommonDivisor(int a, int b);
int findSmallestCommonMultiplier(vector<int> nums);
int findLargestPrimeLessThanGivenNumber(int limit);
double findValueOfPie(int nSamples);
