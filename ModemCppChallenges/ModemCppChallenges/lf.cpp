#include "lf.h"

ipv4::ipv4():sections{{0}}
{
}

//https://en.cppreference.com/w/cpp/container/array
//double brace sections{{a,b,c,d}} required in c++11
// double braces never required after =, ie std::array<int, 4> sections = {1,2,3,4} 
ipv4::ipv4(unsigned char a, unsigned char b, unsigned char c, unsigned char d):
	sections{a,b,c,d}{}

//both default copy constructor and copy assignment should work without the following
//overrides
ipv4::ipv4(ipv4 const & other): sections(other.sections) {}

ipv4::ipv4(int decimal_val) {
	int section_1 = 0,
		section_2 = 0,
		section_3 = 0,
		section_4 = 0;
	int remainder = decimal_val;

	section_1 = remainder / (256 * 256 * 256);
	
	if (section_1 > 0)
		remainder = remainder % (256 * 256 * 256);

	section_2 = remainder / (256 * 256);

	if (section_2 > 0)
		remainder = remainder % (256 * 256);

	section_3 = remainder / (256);

	section_4 = remainder % (256);

	sections = { (unsigned char) section_1 , 
		(unsigned char) section_2, 
		(unsigned char) section_3, 
		(unsigned char) section_4 };
}

ipv4& ipv4::operator=(ipv4 const& other)
{
	sections = other.sections;
	return *this;
}

void ipv4::update_section(int idx, char val)
{
	sections[idx] = val;
}

int ipv4::section(int idx)
{
	return sections[idx];
}

int ipv4::to_base_10()
{
	return sections[0] * 256 * 256 * 256 +
		   sections[1] * 256 * 256 + 
		   sections[2] * 256 +
		   sections[3];
}








void ipv4::update_section(std::string str_ip)
{
	//https://stackoverflow.com/questions/5167625/splitting-a-c-stdstring-using-tokens-e-g

	std::istringstream str_str{str_ip};
	char d1, d2, d3;
	int b1, b2, b3, b4;
	str_str >> b1 >> d1 >> b2 >> d2 >> b3 >> d3 >> b4;

	if (d1 == '.' && d2 == '.' && d3 == '.') {
		update_section(0, b1);
		update_section(1, b2);
		update_section(2, b3);
		update_section(3, b4);
	}
}





std::ostream& operator<<(std::ostream& os, const ipv4& a)
{
	os << static_cast<int>(a.sections[0]) << '.'
		<< static_cast<int>(a.sections[1]) << '.'
		<< static_cast<int>(a.sections[2]) << '.'
		<< static_cast<int>(a.sections[3]);
	return os;
}

std::istream& operator>>(std::istream& is, ipv4& a)
{
	// TODO: insert return statement here
	char d1, d2, d3;
	int b1, b2, b3, b4;
	is >> b1 >> d1 >> b2 >> d2 >> b3 >> d3 >> b4;
	
	if (d1 == '.' && d2 == '.' && d3 == '.') {
		a.update_section(0, b1);
		a.update_section(1, b2);
		a.update_section(2, b3);
		a.update_section(3, b4);
	}
	else {
		is.setstate(std::ios_base::failbit);
	}
	return is;

}

void list_ips(ipv4 from_ip, ipv4 to_ip)
{
	for (int l1 = from_ip.to_base_10(); l1 <= to_ip.to_base_10(); l1++) {
		std::cout << "ip : " << ipv4(l1) << std::endl;
	}
}

