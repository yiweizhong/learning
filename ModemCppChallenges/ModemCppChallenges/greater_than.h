#pragma once

class greater_than {
private:
	int m_value;

public:
	greater_than(int value) : m_value(value) {

	}
	bool operator()(int arg) const {
		return arg > m_value;
	}
};