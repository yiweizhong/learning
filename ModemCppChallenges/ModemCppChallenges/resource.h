#pragma once

#include <vector>
#include <stdlib.h>
#include <array>
#include <iostream>
#include <sstream>

class resource {
private:
	std::string name;
public:
	resource() = delete;
	resource(resource& other) = delete;
	resource& operator=(resource& other) = delete;
	resource(resource&& other) = delete;
	resource& operator=(resource&& other) noexcept;
	resource(const std::string& name);
	~resource();
};

class resource_wrapper {
private:
	resource* rsc = new resource("resource default");
public:
	resource_wrapper(const std::string& name);
	resource_wrapper(resource&& other);
	~resource_wrapper();

};

