
#include <windows.h>
#include <math.h>
#include "xlcall.h"
#include "framework.h"

#include "xllTest/blackScholes.h"

//	Wrappers

extern "C" __declspec(dllexport)
FP12 * xMySequence(double row, double col)
{
	size_t resultRows = row, resultCols = col, resultSize = resultRows * resultCols;

	// First, free all memory previously allocated
	// the function is defined in framework.h
	FreeAllTempMemory();

	// Then, allocate the memory for this result
	// We don't need to de-allocate it, that will be done by the next call
	// to this function or another function calling FreeAllTempMemory()

	// Memory size required, details in Dalton's book, section 6.2.2
	size_t memSize = sizeof(FP12) + (resultSize - 1) * sizeof(double);

	// Finally allocate, function definition in framework.h
	FP12* result = (FP12*)GetTempMemory(memSize);

	// Compute result
	result->rows = resultRows;
	result->columns = resultCols;
	double temp = 0;
	for (size_t i = 0; i < resultRows; ++i) 
		for (size_t j = 0; j < resultCols; ++j)
		{
			result->array[i * resultCols + j] = temp++;
		}


	return result;
}

extern "C" __declspec(dllexport)
double xRangeFunc(FP12 * matrix, double scalar)
{
	// Unpack matrix
	size_t rows = matrix->rows;
	size_t cols = matrix->columns;
	double* numbers = matrix->array;
	// Compute result
	double result = 0.0;
	for (size_t j = 0; j < cols; ++j)
	{
		double norm = 0.0;
		for (size_t i = 0; i < rows; ++i)
		{
			// To access matrix[i][j] = numbers[i*cols+j]
			double xij = numbers[i * cols + j];
			norm += xij * xij;
		}
		norm = sqrt(norm);
		result += (j + 1) * norm;
	}
	result *= scalar;
	return result;
}

extern "C" __declspec(dllexport)
double xMultiply2Numbers(double x, double y)
{
	return x * y;
}

extern "C" __declspec(dllexport)
double xBlackScholes(double spot, double vol, double mat, double strike)
{
	return blackScholes(spot, vol, mat, strike);
}

//	Registers

extern "C" __declspec(dllexport) int xlAutoOpen(void)
{
	XLOPER12 xDLL;	

	Excel12f(xlGetName, &xDLL, 0);

	Excel12f(xlfRegister, 0, 11, (LPXLOPER12)&xDLL,
		(LPXLOPER12)TempStr12(L"xMySequence"),
		(LPXLOPER12)TempStr12(L"K%BB"),
		(LPXLOPER12)TempStr12(L"xMySequence"),
		(LPXLOPER12)TempStr12(L"row_size, col_size"),
		(LPXLOPER12)TempStr12(L"1"),
		(LPXLOPER12)TempStr12(L"myOwnCppFunctions"),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L"Generate sequece for matrix"),
		(LPXLOPER12)TempStr12(L""));

	Excel12f(xlfRegister, 0, 11, (LPXLOPER12)&xDLL,
		(LPXLOPER12)TempStr12(L"xRangeFunc"),
		(LPXLOPER12)TempStr12(L"BK%B"),
		(LPXLOPER12)TempStr12(L"xRangeFunc"),
		(LPXLOPER12)TempStr12(L"matrix, y"),
		(LPXLOPER12)TempStr12(L"1"),
		(LPXLOPER12)TempStr12(L"myOwnCppFunctions"),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L"Range function"),
		(LPXLOPER12)TempStr12(L""));


	Excel12f(xlfRegister, 0, 11, (LPXLOPER12)&xDLL,
		(LPXLOPER12)TempStr12(L"xMultiply2Numbers"),
		(LPXLOPER12)TempStr12(L"BBB"),
		(LPXLOPER12)TempStr12(L"xMultiply2Numbers"),
		(LPXLOPER12)TempStr12(L"x, y"),
		(LPXLOPER12)TempStr12(L"1"),
		(LPXLOPER12)TempStr12(L"myOwnCppFunctions"),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L"Multiplies 2 numbers"),
		(LPXLOPER12)TempStr12(L""));

	Excel12f(xlfRegister, 0, 11, (LPXLOPER12)&xDLL,
		(LPXLOPER12)TempStr12(L"xBlackScholes"),
		(LPXLOPER12)TempStr12(L"BBBBB"),
		(LPXLOPER12)TempStr12(L"xBlackScholes"),
		(LPXLOPER12)TempStr12(L"spot, vol, mat, strike"),
		(LPXLOPER12)TempStr12(L"1"),
		(LPXLOPER12)TempStr12(L"myOwnCppFunctions"),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L""),
		(LPXLOPER12)TempStr12(L"Implements Black & Scholes formula"),
		(LPXLOPER12)TempStr12(L""));

	
	/* Free the XLL filename */
	Excel12f(xlFree, 0, 1, (LPXLOPER12)&xDLL);

	return 1;
}

