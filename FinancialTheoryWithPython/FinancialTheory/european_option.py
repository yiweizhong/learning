import math
import numpy as np
import numpy.random as npr  
import scipy.stats as scs
from pylab import plt, mpl


def gen_sn(M, I, anti_paths=True, mo_match=True):
        ''' Function to generate random numbers for simulation.

        Parameters
        ==========
        M: int
            number of time intervals for discretization
        I: int
            number of paths to be simulated
        anti_paths: boolean
            use of antithetic variates
        mo_math: boolean
            use of moment matching
        '''
        if anti_paths is True:
            sn = npr.standard_normal((M + 1, int(I / 2)))
            sn = np.concatenate((sn, -sn), axis=1)
        else:
            sn = npr.standard_normal((M + 1, I))
        if mo_match is True:
            sn = (sn - sn.mean()) / sn.std()
        return sn
         
#setups            
S0 = 100.
r = 0.05
sigma = 0.25
T = 1.0
I = 50000

'''GBM monte carlo estimation for strikes'''
K=105.

'''
50000 ran number gen however this function actually gen 2 sets of 50000 to 
for other model needs
'''
sn = gen_sn(1, I)

# simulate index level at maturity. There is only 1 time step
ST = S0 * np.exp((r - 0.5 * sigma ** 2) * T + sigma * math.sqrt(T) * sn[1])

# calculate payoff at maturity
hT = np.maximum(ST - K, 0)

# calculate MCS estimator
C0 = math.exp(-r * T) * np.mean(hT)

'''
C0
Out[47]: 10.009064299278853
'''