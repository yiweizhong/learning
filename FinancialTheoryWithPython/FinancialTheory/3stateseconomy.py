'''
in a 3 states economy

Bond value t0 = 10, t1 = {11, 11, 11}
equity value t0 = 10, t1 = {20, 10, 5}

'''

import numpy as np
from pylab import mpl, plt
from mpl_toolkits.mplot3d import Axes3D
np.set_printoptions(precision=5)

#seed
np.random.seed(100)

'''
Bond 
Out[4]: (10, array([11, 11, 11]))
'''
B = (10, np.array((11, 11, 11)))

'''
Stock
(10, array([20, 10,  5]))
'''
S = (10, np.array((20, 10, 5)))


'''
simulate 1000 portfolios with 
the bond weight for these portfolios as b
the stock weight for these portfolios as s
'''
n = 1000
b = np.random.random(n)
s = np.random.random(n)


'''
A is a list of all the portfolios' 3 end values. 
Each one is for 1 of the 3 possible states.

for example:
 array([5.31096, 4.25849, 3.73225]),
 array([21.27752, 11.46854,  6.56405]),
 array([7.70221, 4.05423, 2.23024]),
 array([22.1489 , 14.36325, 10.47042])]

'''

A = [b[i] * B[1] + s[i] * S[1] for i in range(n)]
A = np.array(A) # covnvert the list of numpy array into numpy 2d array

'''
a 3d pay off diagrams with each axis representing 1 state and its payoff
'''

fig = plt.figure(figsize=(10, 6))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(A[:, 0], A[:, 1], A[:, 2], c='r', marker='.')