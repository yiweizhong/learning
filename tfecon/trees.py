import pandas as pd
import tensorflow as tf


#Prepare data for use in gradient boosted classification trees 

# Load hmda data using pandas.
hmda = pd.read_csv('./data/hmda.csv').dropna()
hmda['applicant_income_000s'] = hmda['applicant_income_000s'] * 1000
# Define applicant income feature column.
applicantIncome = tf.feature_column.numeric_column("applicantIncome")
# Define applicant msa relative income.
areaIncome = tf.feature_column.numeric_column("areaIncome")
# Combine features into list.
feature_list = [applicantIncome, areaIncome]


# Define input data function to generate data.

def input_fn():
        # Define dictionary of features.
        features = {"applicantIncome": hmda['applicant_income_000s'],
        "areaIncome": hmda['hud_median_family_income']}
        # Define labels.
        labels = hmda['accepted'].copy()
        # Return features and labels.
        return features, labels
    
# Define boosted trees classifier.
model = tf.estimator.BoostedTreesClassifier(
    feature_columns = feature_list,
    n_batches_per_layer = 1)

# Train model using 100 epochs.
model.train(input_fn, steps=100)

# Evaluate model in-sample.
result = model.evaluate(input_fn, steps = 1)

# Print results.
print(pd.Series(result))


#Prepare data for use in gradient boosted regression trees - Y is continuous


# Load hmda data using pandas.
hmda = pd.read_csv('./data/hmda.csv').dropna()
hmda['applicant_income_000s'] = hmda['applicant_income_000s'] * 1000
# Define applicant income feature column.
applicantIncome = tf.feature_column.numeric_column("applicantIncome")
# Define applicant msa relative income.
areaIncome = tf.feature_column.numeric_column("areaIncome")
# Combine features into list.
feature_list = [applicantIncome, areaIncome]



def input_fn_regression():
    features = {"applicantIncome": hmda['applicant_income_000s'],
                "areaIncome": hmda['hud_median_family_income']}
    targets = hmda['loan_amount'].copy() 
    return features, targets

# Define model.
model = tf.estimator.BoostedTreesRegressor(
        feature_columns = feature_list,
        n_batches_per_layer = 1)

# Train model using 100 epochs.
model.train(input_fn_regression, steps=100)

# Evaluate model in-sample.
result = model.evaluate(input_fn_regression, steps = 1)
# Print results.
print(pd.Series(result))