import tensorflow as tf
print(tf.__version__)



'OLS through math'

' (X' X)^-1 X' Y
 
 
x = tf.constant([[1,0],[1,2]], tf.float32)
y = tf.constant([[2],[4]], tf.float32)

#matrix multiply x by x'
beta0 = tf.linalg.inv(tf.matmul(tf.transpose(x), x))
beta1 = tf.matmul(beta0, tf.transpose(x))
beta = tf.matmul(beta1, y)

#v2
print(x)
print(beta0)
print(beta.numpy())
print(beta)


'OLS through Kera

# Define sequential model.
ols = tf.keras.Sequential()
# Add dense layer with linear activation.
ols.add(tf.keras.layers.Dense(1, input_shape = (2,), use_bias = False, 
                              activation = 'linear'))
# Set optimizer and loss.
ols.compile(optimizer = 'SGD', loss = 'mse')

# Train model for 500 epochs.
ols.fit(x, y, epochs = 500)

print(ols.weights[0].numpy())


'OLS through tf.estimator

# Define feature columns.
features = [
tf.feature_column.numeric_column("constant"),
tf.feature_column.numeric_column("x1")
]

# Define model.
ols = tf.estimator.LinearRegressor(features)

# Define function to feed data to model.
def train_input_fn():
        features = {"constant": [1, 1], "x1": [0, 2]}
        target = [2, 4]
        return features, target
# Train OLS model.
ols.train(train_input_fn, steps = 100)


'add'
import tensorflow as tf
# Define two scalars as constants.
s1 = tf.constant(5, tf.float32)
s2 = tf.constant(15, tf.float32)
# Add and multiply using tf.add() and tf.multiply().
s1s2_sum = tf.add(s1, s2)
s1s2_product = tf.multiply(s1, s2)
# Add and multiply using operator overloading.
s1s2_sum = s1+s2
s1s2_product = s1*s2
# Print sum.
print(s1s2_sum)
#tf.Tensor(20.0, shape=(), dtype=tf.float32)
# Print product.
print(s1s2_product)
#tf.Tensor(75.0, shape=(), dtype=float32)



'element multiplication'

import tensorflow as tf
# Generate 6-tensors from normal distribution draws.
A = tf.random.normal([5, 10, 7, 3, 2, 15])
B = tf.random.normal([5, 10, 7, 3, 2, 15])
# Perform elementwise multiplication.
C = tf.multiply(A, B)
C = A*B


'dot mulpiplication'
import tensorflow as tf
# Set random seed to generate reproducible results.
tf.random.set_seed(1)
# Use normal distribution draws to generate tensors.
A = tf.random.normal([200])
B = tf.random.normal([200])
# Perform dot product.
c = tf.tensordot(A, B, axes = 1)
# Print numpy argument of c.
print(c.numpy())
#-15.284362


'matrix multiplication'
import tensorflow as tf
# Use normal distribution draws to generate tensors.
A = tf.random.normal([200, 50])
B = tf.random.normal([50, 10])
# Perform matrix multiplication.
C = tf.matmul(A, B)
# Print shape of C.
print(C.shape)



'generate random data to train LAD'

import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Set number of observations and samples
S = 100
N = 10000

# Set true values of parameters.
alpha = tf.constant([1.], tf.float32)
beta = tf.constant([3.], tf.float32)

# Draw independent variable and error.
X = tf.random.normal([N, S])
epsilon = tf.random.normal([N, S], stddev=0.25)

# Compute dependent variable.
Y = alpha + beta*X + epsilon


# Draw initial values randomly.
alphaHat0 = tf.random.normal([1], stddev=5.0)
betaHat0 = tf.random.normal([1], stddev=5.0)
# Define variables.
alphaHat = tf.Variable(alphaHat0, tf.float32)
betaHat = tf.Variable(betaHat0, tf.float32)


#this is the loss function
#the alphaHat and betHat are tf variable while the xSample and ySample are 
#constant
def maeLoss(alphaHat, betaHat, xSample, ySample):
    prediction = alphaHat + betaHat*xSample
    error = ySample - prediction
    absError = tf.abs(error)
    return tf.reduce_mean(absError)

# Define optimizer.
opt = tf.optimizers.SGD()
# Define empty lists to hold parameter values.
alphaHist, betaHist = [], []
# Perform minimization and retain parameter updates.
for j in range(1000):
        # Perform minimization step.
        opt.minimize(lambda: maeLoss(alphaHat, betaHat,
        X[:,0], Y[:,0]), var_list = [alphaHat,betaHat])
        # Update list of parameters.
        alphaHist.append(alphaHat.numpy()[0])
        betaHist.append(betaHat.numpy()[0])

# Define DataFrame of parameter histories.

params = pd.DataFrame(np.vstack([alphaHist,betaHist]).transpose(),columns = ['alphaHat', 'betaHat'])

# Generate plot.
params.plot(figsize=(10,7))
# Set x axis label.
plt.xlabel('Epoch')
# Set y axis label.
plt.ylabel('Parameter Value')



'partial OLS'

import tensorflow as tf
# Set number of observations and samples
S = 100
N = 10000
# Set true values of parameters.
alpha = tf.constant([1.], tf.float32)
beta = tf.constant([3.], tf.float32)
theta = tf.constant([0.05], tf.float32)
# Draw independent variable and error.
X = tf.random.normal([N, S])
Z = tf.random.normal([N, S])
epsilon = tf.random.normal([N, S], stddev=0.25)
# Compute dependent variable.
Y = alpha + beta*X + tf.exp(theta*Z) + epsilon

# Draw initial values randomly.
alphaHat0 = tf.random.normal([1], stddev=5.0)
betaHat0 = tf.random.normal([1], stddev=5.0)
thetaHat0 = tf.random.normal([1], mean=0.05, stddev=0.10)

# Define variables.
alphaHat = tf.Variable(alphaHat0, tf.float32)
betaHat = tf.Variable(betaHat0, tf.float32)
thetaHat = tf.Variable(thetaHat0, tf.float32)
# Compute prediction.
def plm(alphaHat, betaHat, thetaHat, xS, zS):
        prediction = alphaHat + betaHat*xS + \
                        tf.exp(thetaHat*zS)
        return prediction
# Define function to compute MAE loss.
def maeLoss(alphaHat, betaHat, thetaHat, xS, zS, yS):
        yHat = plm(alphaHat, betaHat, thetaHat, xS, zS)
        return tf.losses.mae(yS, yHat)
    

# Instantiate optimizer.
opt = tf.optimizers.SGD()

# Define empty lists to hold parameter values.
alphaHist, betaHist, thetaHist = [], [], []
# Perform optimization.
for i in range(1000):
        opt.minimize(lambda: maeLoss(alphaHat, betaHat,
        thetaHat, X[:,0], Z[:,0], Y[:,0]),
        var_list = [alphaHat, betaHat, thetaHat])
        # Update list of parameters.

        alphaHist.append(alphaHat.numpy()[0])
        betaHist.append(betaHat.numpy()[0])
        thetaHist.append(thetaHat.numpy()[0])
        
        
        

# Define DataFrame of parameter histories.

params = pd.DataFrame(np.vstack([alphaHist,betaHist, thetaHist]).transpose(),
                      columns = ['alphaHat', 'betaHat', 'thetaHat'])

# Generate plot.
params.plot(figsize=(10,7))
# Set x axis label.
plt.xlabel('Epoch')
# Set y axis label.
plt.ylabel('Parameter Value')




'non linear regression'

import pandas as pd
import numpy as np
import tensorflow as tf

# Load data.
data = pd.read_csv(r'./data/DEXUSUK.csv')
log_usdgbp = np.log(data['DEXUSUK'].replace('.',np.nan).ffill().astype(float)) 

# Convert log exchange rate to numpy array.
e = np.array(log_usdgbp)
# Identify exchange decreases greater than 2%.
# encode the series with 1 and 0 for the condition used in cast
de = tf.cast(np.diff(e[:-1]) < -0.02, tf.float32)

# Define the lagged exchange rate as a constant.
le = tf.constant(e[1:-1], tf.float32)
# Define the exchange rate as a constant.
e = tf.constant(e[2:], tf.float32)

# Define variables.
rho0Hat = tf.Variable(0.80, tf.float32)
rho1Hat = tf.Variable(0.80, tf.float32)

# Define model.
def tar(rho0Hat, rho1Hat, le, de):
        # Compute regime-specific prediction.
        regime0 = rho0Hat*le
        regime1 = rho1Hat*le
        # Compute prediction for regime.
        # de is 1 or 0 so (1-de) is eseentially regime switching
        prediction = regime0*de + regime1*(1-de)
        return prediction


# Define loss.
def maeLoss(rho0Hat, rho1Hat, e, le, de):
        ehat = tar(rho0Hat, rho1Hat, le, de)
        return tf.losses.mae(e, ehat)


# Define optimizer.
opt = tf.optimizers.SGD()

rho0HatHist, rho1HatHist = [], []

# Perform minimization.
for i in range(20000):
        opt.minimize(lambda: maeLoss(
        rho0Hat, rho1Hat, e, le, de), var_list = [rho0Hat, rho1Hat])
        rho0HatHist.append(rho0Hat.numpy())
        rho1HatHist.append(rho1Hat.numpy())        


# Define DataFrame of parameter histories.
params = pd.DataFrame(np.vstack([rho0HatHist, rho1HatHist]).transpose(),
                      columns = ['rho0HatHist', 'rho1HatHist'])

# Generate plot.
params.plot(figsize=(10,7))
# Set x axis label.
plt.xlabel('Epoch')
# Set y axis label.
plt.ylabel('Parameter Value')
        
        